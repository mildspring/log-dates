<!DOCTYPE html>
<HTML lang="en">
<HEAD>
<meta charset="utf-8">
<link href="bootstrap/css/bootstrap.css" rel="stylesheet">

<script type="text/javascript">
	function highlightDateField() {
		var starting = parseInt(document.getElementById('datePositionStart').value);
		var ending = parseInt(document.getElementById('datePositionEnd').value);
		if (isNaN(starting)) {
			starting = 0;
		}
		if (isNaN(ending)) {
			ending = 0;
		}
		var presentationTable = document.getElementById('presentationTable');
		var dataTable = document.getElementById('dataTable');
		for (var i = 0, row; row = dataTable.rows[i]; i++) {
		   //iterate through rows
		   //rows would be accessed using the "row" variable assigned in the for loop
		   //for (var j = 0, col; col = row.cells[j]; j++) {
		   //  //iterate through columns
		   //  //columns would be accessed using the "col" variable assigned in the for loop
		   //}
		   var origValue = row.cells[2].innerHTML;
		   // [1+1] - because dataTable does not contain header
		   var outputValue = "";
		   if (starting > 0) {
		   	outputValue += origValue.substring(0, starting);
		   }
		   if (ending <= starting) {
		   	localEnding = origValue.length;
		   }
		   else {
		   	localEnding = ending;
		   }

		   outputValue += '<span class="text-success">' + origValue.substring(starting, localEnding) + '</span>';
		   	outputValue += origValue.substring(localEnding);
			presentationTable.rows[i+1].cells[2].innerHTML = outputValue;

		   //presentationTable.rows[i+1].cells[2].innerHTML = origValue.substring(0, starting) + '<span class="text-success">' + origValue.substring(starting, ending) + '</span>' + origValue.substring(ending);
		   //row.cells[2].innerHTML = 'hello world';
		}
	}
	function datePositionStartKeyUp () {
		var datePositionStart = parseInt(document.getElementById('datePositionStart').value);
		var datePositionEnd = parseInt(document.getElementById('datePositionEnd').value);
		if (datePositionStart + 10 > datePositionEnd) {
			document.getElementById('datePositionEnd').value = datePositionStart+10;
		}
	}
	function datePositionEndKeyUp () {
		var datePositionStart = parseInt(document.getElementById('datePositionStart').value);
		var datePositionEnd = parseInt(document.getElementById('datePositionEnd').value);

		// this doesn't work well, because if user changes from 100 to 110, they will first type in '1', then '1', then '1'. The first '1', will make value of field '1', so 'start' will reset
		//if (datePositionEnd < datePositionStart) {
		//	document.getElementById('datePositionStart').value = Math.max(datePositionEnd - 10, 0);
		//}
	}
</script>

</HEAD>
<BODY>
<TABLE class="table table-striped" id="presentationTable">
<thead>
<tr><th>time difference</th><th>timestamp</th><th>text</th></tr>
</thead>
<tbody>
<TR>
<?php
$array = array(
"6/16/2013 3:08:58 PM Starting test",
"6/16/2013 3:08:58 PM part 1 done",
"6/16/2013 3:09:21 PM part 2 done",
"6/16/2013 3:10:49 PM part 3 done",
"6/16/2013 3:10:49 PM part 4 done",
"6/16/2013 3:10:49 PM part 5 done",
"6/16/2013 3:10:49 PM all done");

$array3 = array(
"6/4/2013 3:32:50 PM starting test",
"6/4/2013 3:32:50 PM part 1 done",
"6/4/2013 3:33:39 PM part 2 done",
"6/4/2013 3:36:48 PM part 3 done",
"6/4/2013 3:36:48 PM part 4 done",
"6/4/2013 3:36:48 PM part 5 done",
"6/4/2013 3:36:48 PM part 6 done");

if ($_POST["textarea"])
{
	$array = explode("\n", $_POST["textarea"]);
}


function PrintDiff($date1, $date2)
{
	print_r("<th>");
	if ($date1 && $date2)
	{
		$interval = date_diff($date1, $date2);
		if ($interval->y > 0)
		{
			print_r($interval->y . "years ");
		}
		if ($interval->m > 0)
		{
			print_r($interval->m . "months ");
		}
		if ($interval->d > 0)
		{
			print_r($interval->d . "days ");
		}
		if ($interval->h > 0)
		{
			print_r($interval->h . "h ");
		}
		if ($interval->i > 0)
		{
			print_r($interval->i . "m ");
		}
		if ($interval->s > 0)
		{
			print_r($interval->s . "s ");
		}
	}
	print_r("</th>\n");
}

// from: http://stackoverflow.com/questions/5830387/how-to-find-all-youtube-video-ids-in-a-string-using-a-regex
function linkifyYouTubeURLs($text) {
    $text = preg_replace('~
        # Match non-linked youtube URL in the wild. (Rev:20111012)
        https?://         # Required scheme. Either http or https.
        (?:[0-9A-Z-]+\.)? # Optional subdomain.
        (?:               # Group host alternatives.
          youtu\.be/      # Either youtu.be,
        | youtube\.com    # or youtube.com followed by
          \S*             # Allow anything up to VIDEO_ID,
          [^\w\-\s]       # but char before ID is non-ID char.
        )                 # End host alternatives.
        ([\w\-]{11})      # $1: VIDEO_ID is exactly 11 chars.
        (?=[^\w\-]|$)     # Assert next char is non-ID or EOS.
        (?!               # Assert URL is not pre-linked.
          [?=&+%\w]*      # Allow URL (query) remainder.
          (?:             # Group pre-linked alternatives.
            [\'"][^<>]*>  # Either inside a start tag,
          | </a>          # or inside <a> element text contents.
          )               # End recognized pre-linked alts.
        )                 # End negative lookahead assertion.
        [?=&+%\w-]*        # Consume any URL (query) remainder.
        ~ix',
        '<a href="http://www.youtube.com/watch?v=$1">YouTube link: $1</a>',
        $text);
    return $text;
}

function linkifyYouTubeURLs2($text) {
    $text = preg_replace('~
        # Match non-linked youtube URL in the wild. (Rev:20111012)
        https?://         # Required scheme. Either http or https.
        (?:[0-9A-Z-]+\.)? # Optional subdomain.
        (?:               # Group host alternatives.
          youtu\.be/      # Either youtu.be,
        | youtube\.com    # or youtube.com followed by
          \S*             # Allow anything up to VIDEO_ID,
          [^\w\-\s]       # but char before ID is non-ID char.
        )                 # End host alternatives.
        ([\w\-]{11})      # $1: VIDEO_ID is exactly 11 chars.
        (?=[^\w\-]|$)     # Assert next char is non-ID or EOS.
        (?!               # Assert URL is not pre-linked.
          [?=&+%\w]*      # Allow URL (query) remainder.
          (?:             # Group pre-linked alternatives.
            [\'"][^<>]*>  # Either inside a start tag,
          | </a>          # or inside <a> element text contents.
          )               # End recognized pre-linked alts.
        )                 # End negative lookahead assertion.
        [?=&+%\w-]*        # Consume any URL (query) remainder.
        ~ix',
        'http://www.youtube.com/embed/$1',
        $text);
    return $text;
}

$first_date="";
$prev_date="";
foreach ($array as &$line)
{
	if (!empty($line) && trim($line) !== '')
	{
		try {
			//print_r(date_parse($line));
			//$current_date=date_parse($line);
			$date_array = date_parse($line);
			$date = new DateTime($date_array["year"] . "-" . $date_array["month"] . "-" . $date_array["day"] . " " . $date_array["hour"] . ":" . $date_array["minute"] . ":" . $date_array["second"], new DateTimeZone('America/New_York'));
			//print_r($date);
			//print($line);

			PrintDiff($date, $prev_date);
			print_r("<th>" . $date->format('Y-m-d H:i:s') . "</th>");
			if (! $first_date)
			{
				$first_date = $date;
			}
		}
		catch (Exception $exception) {
			print_r("<th></th><th></th>");
		}

		print_r("<th>" . $line . "</th></TR><TR>");
		$prev_date=$date;
	}
}
if ($first_date)
{
	PrintDiff($first_date, $date);
	print_r("<th></th><th>Total</th></TR><TR>");
}
?>
</TR>
</tbody>
</TABLE>

<table id="dataTable" border="1" style="display:none">
<tbody>
<TR>
<?php
foreach ($array as &$line)
{
	if (!empty($line) && trim($line) !== '')
	{
		try {
			//print_r(date_parse($line));
			//$current_date=date_parse($line);
			$date_array = date_parse($line);
			$date = new DateTime($date_array["year"] . "-" . $date_array["month"] . "-" . $date_array["day"] . " " . $date_array["hour"] . ":" . $date_array["minute"] . ":" . $date_array["second"], new DateTimeZone('America/New_York'));
			//print_r($date);
			//print($line);

			PrintDiff($date, $prev_date);
			print_r("<th>" . $date->format('Y-m-d H:i:s') . "</th>");
			if (! $first_date)
			{
				$first_date = $date;
			}
		}
		catch (Exception $exception) {
			print_r("<th></th><th></th>");
		}

		print_r("<th>" . $line . "</th></TR><TR>");
		$prev_date=$date;
	}
}
?>
</TR>
</tbody>
</table>

<div class="input-append">
	You can enter start and end positions of date:
  <input class="span2" id="datePositionStart" type="text" onkeyup="datePositionStartKeyUp()">
  <input class="span2" id="datePositionEnd" type="text" onkeyup="datePositionEndKeyUp()">
  <button class="btn" type="button" onmousedown="highlightDateField()">Go!</button>
</div>

<form class="well" action="parse_date.php" method="post">
		<fieldset>
          <div class="control-group">
            <label class="control-label" for="textarea">Textarea</label>
            <div class="controls">
              <textarea class="input-xlarge" name="textarea" rows="6"></textarea>
            </div>
          </div>
          <div class="form-actions">
            <button type="submit" class="btn btn-primary">Process</button>
            <!-- <button class="btn">Cancel</button> -->
          </div>
        </fieldset>
</form>

<PRE>
For example, enter:
6/4/2013 3:32:50 PM starting test
6/4/2013 3:32:50 PM part 1 done
6/4/2013 3:33:39 PM part 2 done
6/4/2013 3:36:48 PM part 3 done
6/4/2013 3:36:48 PM part 4 done
6/4/2013 3:36:48 PM part 5 done
6/4/2013 3:36:48 PM part 6 done
</PRE>


<!-- <iframe width="560" height="315" src="http://www.youtube.com/embed/s6KWnVh-JDs" frameborder="0" allowfullscreen></iframe> -->
<!-- <iframe width="560" height="315" src="<a href="http://www.youtube.com/watch?v=s6KWnVh-JDs">YouTube link: s6KWnVh-JDs</a>" frameborder="0" allowfullscreen></iframe> -->
     <!-- <iframe width="560" height="315" src="http://www.youtube.com/embed/s6KWnVh-JDs" frameborder="0" allowfullscreen></iframe> -->
<!-- <iframe width="560" height="315" src="http://www.youtube.com/embed/s6KWnVh-JDs" frameborder="0" allowfullscreen></iframe> -->
<!-- http://youtu.be/s6KWnVh-JDs -->
<?php
/*
$link = "http://youtu.be/s6KWnVh-JDs";
$real_link = linkifyYouTubeURLs($link);
print_r($real_link);
*/
$link = "http://youtu.be/s6KWnVh-JDs";

$real_link = linkifyYouTubeURLs2($link);

print_r('<iframe width="560" height="315" src="' . $real_link . '" frameborder="0" allowfullscreen></iframe>');
?>

</BODY>
</HTML>
