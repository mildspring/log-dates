// for debugging
var debugDateBeforeSplit = null;
var debugDateAfterSplit = null;
var debugDateAfterTable = null;
var debugDateAfterPopulate = null;
var debugDateAfterParse = null;
var debugDateAfterTimeDiff = null;

// not debug
var thresholdMS = 0;

// shows tooltips
$(function() {
	$("a[rel='tooltip']").tooltip();
	//$('.dropdown-toggle').dropdown();
	$("#OnlyForRegex").hide();
});

// changes button text to selected item
$(function(){
	$(".dropdown-menu li a").click(function(){
		$("#ProcesssButton").text($(this).text());

		if ($("#ProcesssButton").text() == "Regular Expression") {
			$("#OnlyForRegex").show();
		}
		else {
			$("#OnlyForRegex").hide();
		}
	});
});


function getSelectionText() {
	var text = "";
	if (window.getSelection) {
		text = window.getSelection().toString();
	}
	else if (document.selection && document.selection.type != "Control") {
		text = document.selection.createRange().text;
	}
	return text;
}
function getRangeObject(selectionObject) {
	if (selectionObject.getRangeAt)
		return selectionObject.getRangeAt(0);
	else { // Safari!
		var range = document.createRange();
		range.setStart(selectionObject.anchorNode,selectionObject.anchorOffset);
		range.setEnd(selectionObject.focusNode,selectionObject.focusOffset);
		return range;
	}
}
function buttonChanged(rowNum, val)
{
	$("#rowButton" + rowNum).closest(".btn-group").find("button").text(val);
	calculateTimeDiff(thresholdMS);
}
function addDateButton(i, d) {	
	//row.cells[1].innerHTML = '<div class="btn-group">' +
	$("#presentationTable tr:eq(" + i + ") th:eq(1)").html('<div class="btn-group">' +
		'	<button class="btn btn-primary dropdown-toggle" id="rowButton' + i + '" data-toggle="dropdown" rel="tooltip" title="' + d + '">Date<span class="caret"></span></button>' +
		'	<ul class="dropdown-menu">' +
		'		<li><a href="#" onmousedown="buttonChanged(' + i + ', ' + "'Date'" + ')">Date</a></li>' +
		'		<li class="divider"></li>' +
		'		<li><a href="#" onmousedown="buttonChanged(' + i + ', ' + "'Ignore'" + ')">Ignore</a></li>' +
		'</ul>' +
//			'</div>');
'<div class="btn-group">' +
'<a class="btn" href="#" rel="tooltip" title="previous" onmousedown="goUpFrom(' + i + ')"><i class="icon-hand-up"></i></a>' +
'<a class="btn" href="#" rel="tooltip" title="next"  onmousedown="goDownFrom(' + i + ')"><i class="icon-hand-down"></i></a>' +
'</div>' +
		'</div>');
}
function goUpFrom(from) {
	var presentationTable = document.getElementById('presentationTable');
	var found = false;
	for (var i = from-1, row; i >= 0 && !found; i--) {
		row = presentationTable.rows[i];
		if ($("#rowButton" + i) && $("#rowButton" + i).length > 0) {
			if ($("#rowButton" + i).closest(".btn-group").find("button").text() != 'Ignore' && $("#rowButton" + i).closest("tr").find("th:eq(0)").text()) {
				found = true;
				scrollIntoView(i);
			}
		}
	}
	if (!found) {
		scrollIntoView(0);
	}
}
function goDownFrom(from) {
	var presentationTable = document.getElementById('presentationTable');
	var found = false;
	for (var i = from+1, row; row = presentationTable.rows[i] && !found; i++) {
		if ($("#rowButton" + i) && $("#rowButton" + i).length > 0) {
			if ($("#rowButton" + i).closest(".btn-group").find("button").text() != 'Ignore' && $("#rowButton" + i).closest("tr").find("th:eq(0)").text()) {
				found = true;
				scrollIntoView(i);
			}
		}
	}
	if (!found) {
		scrollIntoView(presentationTable.rows.length-1);
	}
}
function processTime(d, j, addButton) {
	if (!isNaN((new Date(d)).getMonth())) { // check for invalid date
		if (addButton) {
			addDateButton(j, d);
		}
		return 1;
	}
	return 0;
}
function regularParsing(lines, linesTotal, addButton) {
	var start=$('textarea').getSelection().start;
	var end=$('textarea').getSelection().end;

	var gotDates = 0;
	if (start < end) {
		var lineNumber = ($('textarea').val().substring(0, start).match(/\n/g)||[]).length;
		var offsetStart
		var offsetEnd;
		if (lineNumber == 0) {
			offsetStart = start;
			offsetEnd = end;
		}
		else {
			offsetStart = start - ($('textarea').val().substring(0, start).lastIndexOf("\n") + 1);
			offsetEnd = end - (start - offsetStart);
			//var line=$('textarea').val().split('\n')[lineNumber-1];
			//offsetStart = start - (line.length + lineNumber * 1);
			//offsetEnd = end - (start - offsetStart);
		}

		var presentationTable = document.getElementById('presentationTable');
		for (var i = 0, j = 1, row; i < linesTotal; i++) {
			if (lines[i].toString().length > 0) {
				var origValue = lines[i];
				var d = origValue.substring(offsetStart, offsetEnd);
				gotDates += processTime(d, j, addButton);
				j++;
			}
		}
	}
	return gotDates;
}
function parseWithRegularExpression(lines, linesTotal, regExp, regOut, addButton) {
	var gotDates = 0;
	for (var i = 0, j = 1; i < linesTotal; i++) {
		if (lines[i].toString().length > 0) {
			var origValue = lines[i];
			if (origValue.match(regExp) != null) {
				var d = origValue.replace(regExp, regOut);
				gotDates += processTime(d, j, addButton);
			}
			j++;
		}
	}
	return gotDates;
}
function doDateProcessing(type, lines, totalLines, addButton) {
	if (type == "Selected Date") {
		return regularParsing(lines, totalLines, addButton);
	}
	else if (type == "G2") {
		return parseWithRegularExpression(lines, totalLines, /^(\d{4})\/(\d\d)\/(\d\d) (\d\d:\d\d:\d\d.\d\d\d).*/, "$1-$2-$3 $4", addButton);
	}
	else if (type == "DataLoader") {
		return parseWithRegularExpression(lines, totalLines, /^(\d[\d]*)\/(\d[\d]*)\/(\d{4}) (\d[\d]*):(\d[\d]*):(\d{2}) (..).*/, "$3-$1-$2 $4:$5:$6:000 $7", addButton);
	}
	else if (type == "AXS-Tracer") {
		return parseWithRegularExpression(lines, totalLines, /^[0-9]([0-9]{4})\-(.{14}).*/, "$1-$2:000", addButton);
	}
	else if (type == "ecellerator") {
		return parseWithRegularExpression(lines, totalLines, /^(\d{4})(\d\d)(\d\d)_(\d\d:\d\d:\d\d).*/, "$1-$2-$3 $4.000", addButton);
	}
	else if (type == "IIS Log") {
		return parseWithRegularExpression(lines, totalLines, /^(\d{4})-(\d\d)-(\d\d) (\d\d:\d\d:\d\d).*/, "$1-$2-$3 $4.000", addButton);
	}
	else if (type == "heroku") {
		return parseWithRegularExpression(lines, totalLines, /^. (\d\d:\d\d:\d\d.\d\d\d) (\d{4})-(\d\d)-(\d\d).*/, "$2-$3-$4 $1", addButton);
	}
	else if (type == "central") {
		return parseWithRegularExpression(lines, totalLines, /^ *Time: (.*) *$/, "$1", addButton);
	}
	else if (type == "Regular Expression") {
		return parseWithRegularExpression(lines, totalLines, eval($("#RegularExpression").val()), $("#RegularOutput").val(), addButton);
	}
	return 0;
}
function isBiggestOf7(val1, val2, val3, val4, val5, val6, val7) {
	if (val1 >= val2 && val1 >= val3 && val1 >= val4 && val1 >= val5 && val1 >= val6 && val1 >= val7) {
		return true;	
	}
	return false;
}
function populatePresentationTable() {
	thresholdMS = 0;
	if ($("#thresholdMilliseconds").val() > 0)
		thresholdMS += $("#thresholdMilliseconds").val();
	if ($("#thresholdSeconds").val() > 0)
		thresholdMS += $("#thresholdSeconds").val() * 1000;
	if ($("#thresholdMinutes").val() > 0)
		thresholdMS += $("#thresholdMinutes").val() * 60 * 1000;
	if ($("#thresholdHours").val() > 0)
		thresholdMS += $("#thresholdHours").val() * 60 * 60 * 1000;
	if ($("#thresholdDays").val() > 0)
		thresholdMS += $("#thresholdDays").val() * 24 * 60 * 60 * 1000;

	$("#presentationTable tbody").empty();
	debugDateBeforeSplit = new Date();
	var lines = $('#textarea').val().split('\n');
	debugDateAfterSplit = new Date();
	for (var i = 0; i < lines.length; i++) {
	//$.each(lines, function(){
		if (lines[i].toString().length > 0) {
			var data = "<tr><th></th><th></th><th></th></tr>";
			$("#presentationTable tbody").append(data);
		}
	}
	$("#presentationTable tbody").append("<tr><th></th><th></th><th>Process Finished</th></tr>");
	debugDateAfterTable = new Date();


	// identifying which text is selected
	//var alextext = getSelectionText();
	//var alexrange = getRangeObject();

	for (var i = 0, j = 1; i < lines.length; i++) {
	//$.each(lines, function(){
		if (lines[i].toString().length > 0) {
			$("#presentationTable tr:eq(" + j + ") th:eq(2)").text(lines[i]);
			j++;
		}
	}
	debugDateAfterPopulate = new Date();

	if ($("#ProcesssButton").text() == "Process") {
		var tryWith = lines.length;
		if (lines.length > 100) {
			tryWith = 100;
		}

		var G2 = doDateProcessing("G2", lines, tryWith, false);
		var DataLoader = doDateProcessing("DataLoader", lines, tryWith, false);
		var AXSTracer = doDateProcessing("AXS-Tracer", lines, tryWith, false);
		var ecellerator = doDateProcessing("ecellerator", lines, tryWith, false);
		var IISLog = doDateProcessing("IIS Log", lines, tryWith, false);
		var heroku = doDateProcessing("heroku", lines, tryWith, false);
		var central = doDateProcessing("central", lines, tryWith, false);
		
		if (isBiggestOf7(G2, DataLoader, AXSTracer, ecellerator, IISLog, heroku, central)) {
			doDateProcessing("G2", lines, lines.length, true);
		}
		else if (isBiggestOf7(DataLoader, G2, AXSTracer, ecellerator, IISLog, heroku, central)) {
			doDateProcessing("DataLoader", lines, lines.length, true);
		}
		else if (isBiggestOf7(AXSTracer, G2, DataLoader, ecellerator, IISLog, heroku, central)) {
			doDateProcessing("AXSTracer", lines, lines.length, true);
		}
		else if (isBiggestOf7(ecellerator, G2, DataLoader, AXSTracer, IISLog, heroku, central)) {
			doDateProcessing("ecellerator", lines, lines.length, true);
		}
		else if (isBiggestOf7(IISLog, G2, DataLoader, AXSTracer, ecellerator, heroku, central)) {
			doDateProcessing("IIS Log", lines, lines.length, true);
		}
		else if (isBiggestOf7(heroku, G2, DataLoader, AXSTracer, ecellerator, IISLog, central)) {
			doDateProcessing("heroku", lines, lines.length, true);
		}
		else if (isBiggestOf7(central, heroku, G2, DataLoader, AXSTracer, ecellerator, IISLog)) {
			doDateProcessing("central", lines, lines.length, true);
		}
	}
	else {
		doDateProcessing($("#ProcesssButton").text(), lines, lines.length, true);
	}
	
	debugDateAfterParse = new Date();

	calculateTimeDiff(thresholdMS);
	debugDateAfterTimeDiff = new Date();
}
function timeDiffBetween(date1Ms, date2Ms, thresholdMS) {
	var differenceMs = date2Ms - date1Ms;
	
	var out = "";
	
	if (thresholdMS < differenceMs) {
		var msDiff = Math.floor(differenceMs % 1000);
		var secDiff = Math.floor((differenceMs / 1000) % 60);
		var minDiff = Math.floor(((differenceMs / 1000) / 60) % 60);
		var hrDiff = Math.floor(((differenceMs / 1000) / 60) / 60 % 24);
		var daysDiff = Math.floor((((differenceMs / 1000) / 60) / 60) / 24);


		if (daysDiff > 0) {
			out += daysDiff + "d ";
		}
		if (hrDiff > 0) {
			out += hrDiff + "h ";
		}
		if (minDiff > 0) {
			out += minDiff + "m ";
		}
		if (secDiff > 0) {
			out += secDiff + "s ";
		}
		if (msDiff > 0) {
			out += msDiff + "ms ";
		}
	}
	return out;
}
function calculateTimeDiff(thresholdMS) {
	var presentationTable = document.getElementById('presentationTable');
	var firstDate;
	var prevDate;
	for (var i = 1, row; row = presentationTable.rows[i]; i++) {
		row.cells[0].innerHTML = "";
		if (i == presentationTable.rows.length-1) {
			if (firstDate && prevDate) {
				var date1Ms = firstDate.getTime();
				var date2Ms = prevDate.getTime();
				row.cells[0].innerHTML = timeDiffBetween(date1Ms, date2Ms, thresholdMS);
			}
		}
		else if ($("#rowButton" + i)) {
			if ($("#rowButton" + i).closest(".btn-group").find("button").text() != 'Ignore') {
				if ($("#rowButton" + i).attr('title') != null) {
					var presentDate = new Date($("#rowButton" + i).attr('title'));
					if (! firstDate) {
						firstDate = presentDate;
					}
					else {
						var date1Ms = prevDate.getTime();
						var date2Ms = presentDate.getTime();
						if (i == (presentationTable.rows.length - 1)) {						
							row.cells[0].innerHTML = timeDiffBetween(date1Ms, date2Ms, 0);
						}
						else {
							row.cells[0].innerHTML = timeDiffBetween(date1Ms, date2Ms, thresholdMS);
						}
					}
					prevDate = presentDate;
				}
			}
		}
	}
}
function scrollIntoView(line) {
	var w = $(window);
	var row = $('#presentationTable').find('tr').eq( line );

	if (row.length){
		$('html,body').animate({scrollTop: row.offset().top - (w.height()/2)}, 1000 );
	}
}
